//--------------------------------------------------------------------------------------------------------//
// Make sphere helper method
//--------------------------------------------------------------------------------------------------------//
function makeSphere(centre, radius, h, v, colour)
{
  var vertexList = [], indexList = [];
  for (var i = 0; i <= v + 1; i++) {
    for (var j = 0; j <= h; j++) {
      var theta = 2 * Math.PI * j / h;
      var y = (i / v - 0.5) * 2;
      var r = Math.sqrt(1 - y * y);
      var x = Math.cos(theta) * r; 
      var z = Math.sin(theta) * r;
      var point = [x, y, z];

      for (var k=0; k<3; k++)
        vertexList[vertexList.length] = point[k] * radius + centre[k];
      for (var k=0; k<3; k++)
        vertexList[vertexList.length] = point[k];
      for (var k=0; k<3; k++)
        vertexList[vertexList.length] = colour[k];

      vertexList[vertexList.length] = j/h;
      vertexList[vertexList.length] = i/v;
  }}
  
  for (var i = 0; i < v; i++) {
    for (var j = 0; j < h; j++) {
      indexList[indexList.length] = i * h + j;
      indexList[indexList.length] = (i + 1) * h + (j + 1) % h;
      indexList[indexList.length] = i * h + (j + 1) % h;
      indexList[indexList.length] = i * h + j;
      indexList[indexList.length] = (i + 1) * h + j;
      indexList[indexList.length] = (i + 1) * h + (j + 1) % h;
  }}

  return {vertex : vertexList, index : indexList};
};

//--------------------------------------------------------------------------------------------------------//
function makeQuad(positions, normals, colours, uvs)
{
  var vertexList = [], indexList = [];

  for (var i = 0; i < 4; ++i)
  {
    for (var k = 0; k<3; ++k)
     vertexList[vertexList.length] = positions[i][k];
    for (var k = 0; k<3; ++k)
     vertexList[vertexList.length] = normals[i][k];
    for (var k = 0; k<3; ++k)
     vertexList[vertexList.length] = colours[i][k];
    for (var k = 0; k<2; ++k)
     vertexList[vertexList.length] = uvs[i][k];
  }

  indexList[indexList.length] = 0;
  indexList[indexList.length] = 1;
  indexList[indexList.length] = 2;
  indexList[indexList.length] = 0;
  indexList[indexList.length] = 2;
  indexList[indexList.length] = 3;

  return {vertex : vertexList, index : indexList};
};

//--------------------------------------------------------------------------------------------------------//
// Convert Base64 encoded images to img objects and add them to DOM
//--------------------------------------------------------------------------------------------------------//
function convertTextures(textureList) {
  for (var e in textureList) {
    var img = document.createElement("img");
    var imgContainer = document.getElementById("imageCollection");
    img.src = textureList[`${e}`];
    imgContainer.appendChild(img);

    textureList[`${e}`] = img;
  };
}

//--------------------------------------------------------------------------------------------------------//
// Program main entry point
//--------------------------------------------------------------------------------------------------------//
var main=function()
{
  // Initialise context (canvas, gl)

  // Get reference to canvas
  var canvas = document.getElementById("canvas");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  canvas.aspect = canvas.width / canvas.height;
  var Vec3 = matrixHelper.vector3;
  var Mat4x4 = matrixHelper.matrix4;
  // Assign context to gl
  var gl = null;
  try { gl = canvas.getContext("experimental-webgl", {antialias: true}); }
  catch (e) {alert("No webGL compatibility detected!"); return false;}

  document.addEventListener('keydown', function(event) {
    switch (event.key.toLowerCase()) {
      case 'q': moveCameraUp();
      break;
      case 'e': moveCameraDown();
      break;
      case 'w': OriginalFixedPoint();
      break;
      case 's': DifferentFixedPoint();
      break;
      case 'r': IncreaseCameraSpeed();
      break;
      case 't': DecreaseCameraSpeed();
      break;
      case 'a': leftMovement();
      break;
      case 'd': rightMovement();
      break;
      case ' ':  if(initialBall){speedIncrease();}
      break;
      case 'x':  if(stickPowerUp && contact){
                    if(ballstick[0]){
                      speeds[0] = speed;
                      thetas[0] = 90;
                    }
                    if(ballstick[1]){
                      speeds[1] = speed;
                      thetas[1] = 90;
                    }

                 }
      break;
    }
  });
  document.addEventListener("keyup",function(event){
    if(event.key === ' ')
      initialBall = false;
  });
  //--------------------------------------------------------------------------------------------------------//
  // Set up scene
  //--------------------------------------------------------------------------------------------------------//
  scene = new Scene();
  scene.initialise(gl, canvas);

  //--------------------------------------------------------------------------------------------------------//
  // Set up geometry
  //--------------------------------------------------------------------------------------------------------//

  var bricks = makeQuad(
    [[-1, -2, 0], [1, -2, 0], [1, 2, 0], [-1, 2, 0]],
    [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
    [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
    [[0,0], [1,0], [1,1], [0,1]]);


  var powerUp = makeQuad([[-1, -1, 0], [1, -1, 0], [1, 1, 0], [-1, 1, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,1,1], [0,1,1], [0,1,1], [0,1,1]],
      [[0,0], [1,0], [1,1], [0,1]]);
  var paddle = makeQuad(
      [[-1, -3, 0], [1, -3, 0], [1, 3, 0], [-1, 3, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);

  var paddleExtended = makeQuad(
      [[-1, -6, 0], [1, -6, 0], [1, 6, 0], [-1, 6, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);


  var border = makeQuad(
      [[-34, -1, 0], [34, -1, 0], [34, 1, 0], [-34, 1, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);

  var borderTop = makeQuad(
      [[-1, -20.5, 0], [1, -20, 0], [1, 20, 0], [-1, 20, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);

  var cannon = makeQuad([[-1, -1, 0], [1, -1, 0], [1, 1, 0], [-1, 1, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,1], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);
  var laser = makeQuad([[-0.7, -0.2, 0], [0.7, -0.2, 0], [0.7, 0.2, 0], [-0.7, 0.2, 0]],
      [[0,0,1], [0,0,1], [0,0,1], [0,0,1]],
      [[0,0,0], [0,0,0], [0,0,0], [0,0,0]],
      [[0,0], [1,0], [1,1], [0,1]]);
  var sphere = makeSphere([0,0,0], 1, 50, 50, [0,0,0]);
  var sphereLives = makeSphere([0,0,0], 1, 50, 50, [0,0,0]);
  //--------------------------------------------------------------------------------------------------------//
  // Set up lights
  //--------------------------------------------------------------------------------------------------------//
  var lights = {};
  lights.lightDirectional = new Light();
  lights.lightDirectional.type = Light.LIGHT_TYPE.DIRECTIONAL;
  lights.lightDirectional.setDiffuse([0.75, 0.75, 0.75]);
  lights.lightDirectional.setSpecular([1, 1, 1]);
  lights.lightDirectional.setAmbient([0.25, 0.25, 0.25]);
  lights.lightDirectional.setDirection([0, 0, -1]);
  lights.lightDirectional.bind(gl, scene.shaderProgram, 0);

  lights.lightPoint = new Light();
  lights.lightPoint.type = Light.LIGHT_TYPE.POINT;
  lights.lightPoint.setPosition([0, 0, 1]);
  lights.lightPoint.setDiffuse([1, 0, 0]);
  lights.lightPoint.setAmbient([0.5, 0, 0]);
  lights.lightPoint.setSpecular([1, 1, 1]);
  lights.lightPoint.setAttenuationType(Light.ATTENUATION_TYPE.LINEAR);
  lights.lightPoint.bind(gl, scene.shaderProgram, 3);



  lights.lightSpotlightOne = new Light();
  lights.lightSpotlightOne.type = Light.LIGHT_TYPE.SPOT;
  lights.lightSpotlightOne.setDirection([0, 1, 0]);
  lights.lightSpotlightOne.setPosition([-0.25, 0, 0]);
  lights.lightSpotlightOne.setDiffuse([1,1,0]);
  lights.lightSpotlightOne.setAmbient([0.5,0.5,0]);
  lights.lightSpotlightOne.setSpecular([1,1,0]);
  lights.lightSpotlightOne.setAttenuationType(Light.ATTENUATION_TYPE.LINEAR);
  lights.lightSpotlightOne.bind(gl, scene.shaderProgram, 1);

  lights.lightSpotlightTwo = new Light();
  lights.lightSpotlightTwo.type = Light.LIGHT_TYPE.SPOT;
  lights.lightSpotlightTwo.setDirection([0, -1, 0]);
  lights.lightSpotlightTwo.setPosition([0.25, 0, 0]);
  lights.lightSpotlightTwo.setDiffuse([1,1,0]);
  lights.lightSpotlightTwo.setAmbient([0.5,0.5,0]);
  lights.lightSpotlightTwo.setSpecular([1,1,0]);
  lights.lightSpotlightTwo.setAttenuationType(Light.ATTENUATION_TYPE.LINEAR);
  lights.lightSpotlightTwo.bind(gl, scene.shaderProgram, 2);
  //--------------------------------------------------------------------------------------------------------//
  // Set up textures and materials
  //--------------------------------------------------------------------------------------------------------//
  var materials = {};




  var textureList = new Textures();
  convertTextures(textureList);

  materials.powerUp = new Material();
  materials.powerUp.setAlbedo(gl, textureList.border);
  materials.powerUp.setSpecular([0,0,0]);
  materials.powerUp.setAmbient([1,1,1]);
  materials.powerUp.setDiffuse([1,1,1]);
  materials.powerUp.bind(gl, scene.shaderProgram);

  materials.border = new Material();
  materials.border.setAlbedo(gl, textureList.border);
  materials.border.setSpecular([0,0,0]);
  materials.border.setAmbient([1,1,1]);
  materials.border.setDiffuse([1,1,1]);
  materials.border.bind(gl, scene.shaderProgram);

  materials.sphere = new Material();
  materials.sphere.setAlbedo(gl, textureList.sphere);
  materials.sphere.setShininess(150.0);
  materials.sphere.setSpecular([1,1,1]);
  materials.sphere.setAmbient([1,1,1]);
  materials.sphere.setDiffuse([1,1,1]);
  materials.sphere.bind(gl, scene.shaderProgram);

  materials.block = new Material();
  materials.block.setAlbedo(gl, textureList.redBlock);
  materials.block.setSpecular([0,0,0]);
  materials.block.setAmbient([1,1,1]);
  materials.block.setDiffuse([1,1,1]);
  materials.block.bind(gl, scene.shaderProgram);

  materials.paddle = new Material();
  materials.paddle.setAlbedo(gl, textureList.paddle);
  materials.paddle.setSpecular([0,0,0]);
  materials.paddle.setAmbient([1,1,1]);
  materials.paddle.setDiffuse([1,1,1]);
  materials.paddle.bind(gl, scene.shaderProgram);

  materials.sphereLives = new Material();
  materials.sphereLives.setAlbedo(gl, textureList.livesSphere);
  materials.sphereLives.setShininess(96.0);
  materials.sphereLives.setSpecular([1,1,1]);
  materials.sphereLives.setAmbient([1,1,1]);
  materials.sphereLives.setDiffuse([1,1,1]);
  materials.sphereLives.bind(gl, scene.shaderProgram);

  materials.cannon = new Material();
  materials.cannon.setAlbedo(gl, textureList.cannoN);
  materials.cannon.setSpecular([0,0,0]);
  materials.cannon.setAmbient([1,1,1]);
  materials.cannon.setDiffuse([1,1,1]);
  materials.cannon.bind(gl, scene.shaderProgram);

  materials.laser = new Material();
  materials.laser.setAlbedo(gl, textureList.laser);
  materials.laser.setSpecular([0,0,0]);
  materials.laser.setAmbient([1,1,1]);
  materials.laser.setDiffuse([1,1,1]);
  materials.laser.bind(gl, scene.shaderProgram);
  var models = {};


  models.powerUp = new Model();
  models.powerUp.name = "powerUp";
  models.powerUp.index = powerUp.index;
  models.powerUp.vertex = powerUp.vertex;
  models.powerUp.material = materials.powerUp;
  models.powerUp.compile(scene);

  models.borderTop = new Model();
  models.borderTop.name = "borderTop";
  models.borderTop.index = borderTop.index;
  models.borderTop.vertex = borderTop.vertex;
  models.borderTop.material = materials.border;
  models.borderTop.compile(scene);


  models.border = new Model();
  models.border.name = "border";
  models.border.index = border.index;
  models.border.vertex = border.vertex;
  models.border.material = materials.border;
  models.border.compile(scene);

  models.block = new Model();
  models.block.name = "block";
  models.block.index = bricks.index;
  models.block.vertex = bricks.vertex;
  models.block.material = materials.block;
  models.block.compile(scene);

  models.sphere = new Model();
  models.sphere.name = "sphere";
  models.sphere.index = sphere.index;
  models.sphere.vertex = sphere.vertex;
  models.sphere.material = materials.sphere;
  models.sphere.compile(scene);

  models.sphereLives = new Model();
  models.sphereLives.name = "sphereLives";
  models.sphereLives.index = sphereLives.index;
  models.sphereLives.vertex = sphereLives.vertex;
  models.sphereLives.material = materials.sphereLives;
  models.sphereLives.compile(scene);


  models.paddle = new Model();
  models.paddle.name = "paddle";
  models.paddle.index = paddle.index;
  models.paddle.vertex = paddle.vertex;
  models.paddle.material = materials.paddle;
  models.paddle.compile(scene);

  models.cannon = new Model();
  models.cannon.name = "cannon";
  models.cannon.index = cannon.index;
  models.cannon.vertex = cannon.vertex;
  models.cannon.material = materials.cannon;
  models.cannon.compile(scene);

  models.laser = new Model();
  models.laser.name = "laser";
  models.laser.index = laser.index;
  models.laser.vertex = laser.vertex;
  models.laser.material = materials.laser;
  models.laser.compile(scene);

  //--------------------------------------------------------------------------------------------------------//
  // Set up scene graph
  //--------------------------------------------------------------------------------------------------------//

  var nodes = {};
  nodes.blocks = [];
  nodes.borders = [];
  nodes.lightDirectional = scene.addNode(scene.root, lights.lightDirectional, "DirectionLightNode", Node.NODE_TYPE.LIGHT);
  nodes.powerUpGroup = scene.addNode(nodes.lightDirectional, null, "powerUpGroup", Node.NODE_TYPE.GROUP);
  nodes.powerups = [];
  nodes.sphereGroups = scene.addNode(scene.root,null,"sphereGroup",Node.NODE_TYPE.GROUP);
  nodes.spheres = [];
  nodes.spheres.push(scene.addNode(scene.root,models.sphere,"Sphere1",Node.NODE_TYPE.MODEL));



  nodes.lightPoint = scene.addNode(nodes.sphereGroups,lights.lightPoint,"PointLightNode",Node.NODE_TYPE.LIGHT);


  nodes.paddle = scene.addNode(scene.root,models.paddle,"Paddle",Node.NODE_TYPE.MODEL);
  nodes.lightSpotOne = scene.addNode(nodes.paddle,lights.lightSpotlightOne,"SpotLightNode1",Node.NODE_TYPE.LIGHT);
  nodes.lightSpotTwo = scene.addNode(nodes.paddle,lights.lightSpotlightTwo,"SpotLightNode2",Node.NODE_TYPE.LIGHT);

  nodes.blockGroup = scene.addNode(nodes.lightDirectional, null, "blockGroup", Node.NODE_TYPE.GROUP);



  for (var i = 0; i< 32; i++){
    nodes.blocks.push(scene.addNode(nodes.blockGroup, models.block, "blocks", Node.NODE_TYPE.MODEL))
  }

  nodes.BorderGroup = scene.addNode(nodes.lightDirectional,null,"BorderGroup",Node.NODE_TYPE.GROUP);
  for (var f = 0; f<2; f++){
    nodes.borders.push(scene.addNode(nodes.BorderGroup,models.border,"Border",Node.NODE_TYPE.MODEL));

  }
    nodes.borders.push(scene.addNode(nodes.BorderGroup,models.borderTop,"Border",Node.NODE_TYPE.MODEL));

  nodes.sphereLivesGroup = scene.addNode(nodes.lightDirectional,null,"SphereLives",Node.NODE_TYPE.GROUP);
  nodes.sphereLives = [];
  for (let e = 0; e < 3; e++){
    nodes.sphereLives.push(scene.addNode(nodes.sphereLivesGroup,models.sphereLives,"SphereLive",Node.NODE_TYPE.MODEL));
  }
  nodes.cannon = scene.addNode(scene.root,models.cannon,"Cannon",Node.NODE_TYPE.MODEL);
  nodes.laserGroup = scene.addNode(scene.root,null,"LaserGroup",Node.NODE_TYPE.GROUP);
  nodes.lasers = [];
  //--------------------------------------------------------------------------------------------------------//
  // Set up animation
  //--------------------------------------------------------------------------------------------------------//

  //this.transform[12] - > Row
  //this.transform[13] - > Column
  var initialBall = true;
  var colCounter = 15;
  var rowCounter = 10;
  var brickBreakPowerUp = false;
  var vausExtendPowerUp = false;
  var slowBallPowerUp = false;
  var slowBallTime = 0;
  var doubleBallPowerUp = false;
  var stickPowerUp = false;
  var contact = false;
  var cannonPowerUp = false;

  var paddlespeed = 0.5;
  var paddlePosition = 0;

  var borderTopData = {};

  nodes.cannon.transform[12] = -12;
  nodes.cannon.transform[13] = 25;
  nodes.borders[0].transform[13] = 20;
  nodes.borders[0].transform[12] = 20;


  nodes.borders[1].transform[13] = -20;
  nodes.borders[1].transform[12] = 20;

  borderTopData.Xposition = 0;
  borderTopData.Yposition = 12.5;
  nodes.borders[2].transform[12] = 12;


  nodes.spheres[0].transform[12] = -10;
  nodes.spheres[0].transform[13] = 0;



  var paddleData = {};
  paddleData.Xposition = 0;
  paddleData.Yposition = -12;
  paddleData.minX = -3;
  paddleData.maxX = 3;
  nodes.paddle.transform[12] = -12;
  nodes.paddle.transform[13] = 0;
  var leftMovement = function(){

    if(!vausExtendPowerUp){
      if(paddlePosition <= 15.5){
        paddlePosition += paddlespeed;
        nodes.paddle.transform[13] = paddlePosition;
        paddleData.Xposition = paddlePosition;
        paddleData.minX = paddlePosition-3;
        paddleData.maxX = paddlePosition+3;
        if(contact){
          if(ballstick[0]){
            nodes.spheres[0].transform[13] +=paddlespeed;
          }
          if(ballstick[1]){
            nodes.spheres[1].transform[13] +=paddlespeed;
          }
          contact = false;
        }
        if(cannonPowerUp){
          nodes.cannon.transform[13] +=paddlespeed;
        }
      }
    }else{
      if(paddlePosition <= 12.5) {
        paddlePosition += paddlespeed;
        nodes.paddle.transform[13] = paddlePosition;
        paddleData.Xposition = paddlePosition;
        paddleData.minX = paddlePosition - 6;
        paddleData.maxX = paddlePosition + 6;
        if(contact){
          if(ballstick[0]){
            nodes.spheres[0].transform[13] +=paddlespeed;
          }
          if(ballstick[1]){
            nodes.spheres[1].transform[13] +=paddlespeed;
          }
          contact = false;
        }
        if(cannonPowerUp){
          nodes.cannon.transform[13] +=paddlespeed;
        }
      }
    }

  };

  var rightMovement = function () {

    if(!vausExtendPowerUp){
      if(paddlePosition >= -15.5){
        paddlePosition -= paddlespeed;
        nodes.paddle.transform[13] = paddlePosition;
        paddleData.Xposition = paddlePosition;
        paddleData.minX = paddlePosition-3;
        paddleData.maxX = paddlePosition+3;
        if(contact){
          if(ballstick[0]){
            nodes.spheres[0].transform[13] -=paddlespeed;
          }
          if(ballstick[1]){
            nodes.spheres[1].transform[13] -=paddlespeed;
          }
          contact = false;
        }
        if(cannonPowerUp){
          nodes.cannon.transform[13] -=paddlespeed;
        }

      }

    }else{
      if(paddlePosition >= -12.5){
        paddlePosition -= paddlespeed;
        nodes.paddle.transform[13] = paddlePosition;
        paddleData.Xposition = paddlePosition;
        paddleData.minX = paddlePosition-6;
        paddleData.maxX = paddlePosition+6;
        if(contact){
          if(ballstick[0]){
            nodes.spheres[0].transform[13] -=paddlespeed;
          }
          if(ballstick[1]){
            nodes.spheres[1].transform[13] -=paddlespeed;
          }
          contact = false;
        }
      }
      if(cannonPowerUp){
        nodes.cannon.transform[13] -=paddlespeed;
      }
    }

  };

  var livesRowCounter = -12;

  for (let h = 0; h <3; h++){
    nodes.sphereLives[h].transform[12] = livesRowCounter;
    livesRowCounter = livesRowCounter + 2.3;
    nodes.sphereLives[h].transform[13] = 22.3;
  }
  var speed = 0.0;

  function float2int (value) {
    return value | 0;
  }


  function toRadians (angle) {
    return angle * (Math.PI / 180);
  }

  function speedIncrease() {
    if(speed < 0.1){
      speed += 0.01;
      speeds = [speed,speed];
    }

  }
  var thetas = [90,0];
  var speeds = [0,0];
  var ballstick = [false,false];
  var lives = 3;
  var ActivePowerBuff = false;
  var lastcheck = 0;
  nodes.sphereGroups.animationCallback = function () {
    if(slowBallPowerUp){
      if((Date.now()/1000 - slowBallTime) > 30){
        speeds = [0.1,0.1];
        slowBallPowerUp = false;
      }
    }
    if(!initialBall){


      for (let y = 0; y<nodes.spheres.length; y++){

        nodes.spheres[y].transform[12] +=speeds[y]*Math.sin(toRadians(thetas[y]));
        nodes.spheres[y].transform[13] +=speeds[y]*Math.cos(toRadians(thetas[y]));
        thetas[y] = circleRectangleCollision(nodes.spheres[y],thetas[y]);
        if(doubleBallPowerUp){
          circleCircleCollision();
        }

      }


      if(ActivePowerBuff){
        for (let i = 0; i<nodes.powerups.length;i++){
          nodes.powerups[i].transform[12] -=0.1;
          rectangleRectangleCollision(i);
        }
      }

      if(cannonPowerUp){
        if((Date.now()/1000 - lastcheck) >2){
          lastcheck = Date.now()/1000;
          let tempLaser = scene.addNode(nodes.laserGroup,models.laser,"laser",Node.NODE_TYPE.MODEL);
          tempLaser.transform[12] = nodes.cannon.transform[12];
          tempLaser.transform[13] = nodes.cannon.transform[13];
          nodes.lasers.push(tempLaser)

        }

        for (let i = 0; i<nodes.lasers.length; i++){
          nodes.lasers[i].transform[12] +=0.1;
          laserRectangleCollision(nodes.lasers[i]);
        }



      }
    }

      if(doubleBallPowerUp){
        if(nodes.spheres[1].transform[12] <= -15 && lives !==-1 && nodes.spheres[0].transform[12] <= -15){
          partialReset();
          lives -=1;
          nodes.sphereLives[lives].transform[12] = 11000000;
          nodes.sphereLives[lives].transform[13] = 11000000;
        }else if((lives-1) === -1){
          alert("OUT OF LIVES GAME WILL RESET FINAL SCORE: "+score);
          location.reload();
        }
      }else{
        if(nodes.spheres[0].transform[12] <= -15 && lives !==-1){
          partialReset();
          lives -=1;
          nodes.sphereLives[lives].transform[12] = 11000000;
          nodes.sphereLives[lives].transform[13] = 11000000;
        }else if((lives-1) === -1){
          alert("OUT OF LIVES GAME WILL RESET FINAL SCORE: "+score);
          location.reload();
        }
      }

      if(score === 32){
        alert("YOU WON!!! CONGRATS");
        location.reload();
      }




  };
  function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  var randomPowerUpNumbers = function () {
    let rangeNum = [];
    for(let u = 0; u <32; u++){
      rangeNum.push(u);
    }
    rangeNum = shuffle(rangeNum);
    let randomRange = [];
    for (let i = 0; i < 6; i++){
      randomRange.push(rangeNum[i]);
    }

    return randomRange;
  };


  var blocksData = {};
  blocksData.blocks = [];
  blocksData.powerUps = randomPowerUpNumbers();
  blocksData.powerUpsData = shuffle(["BB","EV","BS","S","BST","C"]);

  for (var j = 0; j< 32; j++) {
    var block = {};
    block.powerUpCheck = false;
    for (let oo = 0; oo <6; oo++){
      if(j === blocksData.powerUps[oo]){
        block.powerUpCheck = true;
        block.powerUpData = blocksData.powerUpsData[oo];
        break;
      }

    }


    block.Xposition = colCounter;
    block.Yposition = rowCounter;
    block.visibility = true;
    block.minX = colCounter-2;
    block.maxX = colCounter+2;
    block.minY = rowCounter-1;
    block.maxY = rowCounter+1;
    blocksData.blocks.push(block);

    nodes.blocks[j].transform[12] = rowCounter;
    nodes.blocks[j].transform[13] = colCounter;
    if(colCounter === -13){
      colCounter = 15;
      rowCounter -=2;
    }else{
      colCounter = colCounter - 4;
    }

  }

  var circleCircleCollision = function () {
    if(doubleBallPowerUp){
      let x1 = float2int(nodes.spheres[0].transform[13]);
      let x2 = float2int(nodes.spheres[1].transform[13]);
      let y1 = float2int(nodes.spheres[0].transform[12]);
      let y2 = float2int(nodes.spheres[1].transform[12]);
      if(Math.sqrt(Math.pow(x1-x2,2) + Math.pow(y1-y2,2)) === 4){
        thetas[0] = -thetas[0];
        thetas[1] = -thetas[1];

      }

    }

  };


  var laserRectangleCollision = function (laserNode) {
    for (let a = 0; a<32; a++){
      if(float2int(laserNode.transform[12])+2 === blocksData.blocks[a].Yposition){
        if(laserNode.transform[13] >= blocksData.blocks[a].minX && laserNode.transform[13] <= blocksData.blocks[a].maxX && blocksData.blocks[a].visibility){
          nodes.blocks[a].transform[12] = 10000000000;
          nodes.blocks[a].transform[13] = 10000000000;
          blocksData.blocks[a].visibility = false;
          score++;
          laserNode.transform[12] = 100000;
        }


      }

    }


  };
  var score = 0;
  var circleRectangleCollision= function (sphere,theta) {

      let initialContact = false;
      for (let a = 0; a <32; a++){

        if(float2int(sphere.transform[12])+2 === blocksData.blocks[a].Yposition || float2int(sphere.transform[12])-2 === blocksData.blocks[a].Yposition ){

          if(float2int(sphere.transform[13]) >= blocksData.blocks[a].minX && float2int(sphere.transform[13]) <= blocksData.blocks[a].maxX &&
              blocksData.blocks[a].visibility && !initialContact){
               nodes.blocks[a].transform[12] = 10000000000;
               nodes.blocks[a].transform[13] = 10000000000;
               if(!brickBreakPowerUp){
                 theta = -theta;
               }

               blocksData.blocks[a].visibility = false;
               score++;
               initialContact = true;
               if(blocksData.blocks[a].powerUpCheck){
                  ActivePowerBuff = true;
                  let powerTemp = scene.addNode(nodes.powerUpGroup,models.powerUp,blocksData.blocks[a].powerUpData,Node.NODE_TYPE.MODEL);
                  powerTemp.transform[12] =  blocksData.blocks[a].Yposition;
                  powerTemp.transform[13] =  blocksData.blocks[a].Xposition;
                  nodes.powerups.push(powerTemp);
            }
          }

        }

        if(float2int(sphere.transform[13])+3 === blocksData.blocks[a].Xposition && float2int(sphere.transform[13])-3 === blocksData.blocks[a].Xposition){
          if(float2int(sphere.transform[12]) >= blocksData.blocks[a].minY && float2int(sphere.transform[12]) <= blocksData.blocks[a].maxY &&
              blocksData.blocks[a].visibility && !initialContact)
          nodes.blocks[a].transform[12] = 10000000000;
          nodes.blocks[a].transform[13] = 10000000000;
          if(!brickBreakPowerUp){
            theta = -theta;
          }
          blocksData.blocks[a].visibility = false;
          score++;
          initialContact = true;
        }


      }


      if(float2int(sphere.transform[12])+2 === nodes.paddle.transform[12]+4){
        if(sphere.transform[13] >= paddleData.minX && sphere.transform[13] <= paddleData.maxX){
          if(stickPowerUp){
            contact = true;
            if(sphere.name === "Sphere1"){
              speeds[0] = 0.0;
              ballstick[0] = true;
            }else if(sphere.name === "Sphere2"){
              speeds[1] = 0.0;
              ballstick[1] = true;
            }
          }
          if(sphere.transform[13] >= paddleData.minX  &&  sphere.transform[13] <= (paddleData.maxX-2)){
            theta = 140;
          }else if(sphere.transform[13] >= (paddleData.minX+4)  &&  sphere.transform[13] <= (paddleData.maxX)){
            theta = 40;

          }else{
            theta = 90;
          }
        }

      }


      if(float2int(sphere.transform[13])+2 === 20 || float2int(sphere.transform[13])-2 === -20 ){
            theta = 180-theta;
        }else if(float2int(sphere.transform[12])+2 === 12){
        theta = 180-theta;
        //theta = -theta;
        theta = 180+theta;
      }
      return theta;
  };


  var rectangleRectangleCollision = function (nodeIndex) {
    if(float2int(nodes.powerups[nodeIndex].transform[12])+2 === nodes.paddle.transform[12]+4){
      if(nodes.powerups[nodeIndex].transform[13] >= paddleData.minX && nodes.powerups[nodeIndex].transform[13] <= paddleData.maxX){
        nodes.powerups[nodeIndex].transform[13] = 10000000000;
        nodes.powerups[nodeIndex].transform[12] = 10000000000;
        executePowerUp(nodes.powerups[nodeIndex].name);
      }

    }

  };

  //var nowW = Date.now()/1000;
  var executePowerUp = function (name) {

      if(name ==="BB"){
          brickBreakPowerUp = true;
      }else if(name ==="EV"){
        models.paddle.index = paddleExtended.index;
        models.paddle.vertex = paddleExtended.vertex;
        models.paddle.material = materials.paddle;
        models.paddle.compile(scene);
        paddleData.minX = nodes.paddle.transform[13] -6;
        paddleData.maxX = nodes.paddle.transform[13] +6;
        vausExtendPowerUp = true;
        if(cannonPowerUp){
          nodes.cannon.transform[13] = nodes.paddle.transform[13] + paddleData.maxX +1;
        }
      }else if(name ==="BS"){
          doubleBallPowerUp = true;
          nodes.spheres.push(scene.addNode(scene.root,models.sphere,"Sphere2",Node.NODE_TYPE.MODEL));
          thetas[1] = -thetas[0];

      }else if(name ==="S"){
        slowBallTime = Date.now()/1000;
        slowBallPowerUp = true;
        speed = speed/2;
        speeds = [speed,speed];
      }else if(name ==="BST"){
        stickPowerUp = true;
      }else if(name === "C"){
        cannonPowerUp = true;
        nodes.cannon.transform[13] = paddleData.maxX +1;

      }
  };

  var partialReset = function () {
    thetas = [90,0];
    if(!vausExtendPowerUp){
      paddleData.minX = -3;
      paddleData.maxX = 3;


    }else{
      paddleData.minX = -6;
      paddleData.maxX = 6;

    }

    if(doubleBallPowerUp){
      doubleBallPowerUp = false;
      nodes.spheres[1].transform[12] = 1000000000000;
      nodes.spheres[1].transform[13] = 1000000000000;
    }

    nodes.spheres = [];
    nodes.spheres.push(scene.addNode(scene.root,models.sphere,"Sphere1",Node.NODE_TYPE.MODEL));

    paddlePosition = 0;
    paddleData.Xposition = 0;
    paddleData.Yposition = -12;
    nodes.paddle.transform[12] = -12;
    nodes.paddle.transform[13] = 0;
    if(cannonPowerUp){
      nodes.cannon.transform[13] = nodes.paddle.transform[13] + paddleData.maxX +1;
    }
    nodes.spheres[0].transform[12] = -10;
    nodes.spheres[0].transform[13] = 0;
    initialBall = true;
};
  var yCamera = 0;
  var xCamera = 0;
  var cameraSpeed = 0;
  var lightTransform = Mat4x4.create();
  var modelTransform = Mat4x4.create();
  var viewTransform = Mat4x4.create();
  var observer = Vec3.from(yCamera,xCamera,50);


  var IncreaseCameraSpeed = function () {
    if(!(cameraSpeed >= 1.0)){
      cameraSpeed +=0.1;
    }
  };
  var DecreaseCameraSpeed = function () {
    if(!(cameraSpeed <= 0.0)){
      cameraSpeed -=0.1;
    }
  };

  var moveCameraDown = function(){
    if(!(yCamera <= -30.0)){
      yCamera -=cameraSpeed;
      observer = Vec3.from(yCamera,xCamera,50);
    }

  };

  var moveCameraUp = function(){
    if(!(yCamera >= 0.0)){
      yCamera +=cameraSpeed;
      observer = Vec3.from(yCamera,xCamera,50);
    }
  };

  var OriginalFixedPoint = function(){
     yCamera = 0;
     xCamera = 0;
    observer = Vec3.from(0,0,50);
  };

  var DifferentFixedPoint = function(){
    yCamera = -30;
    xCamera = 0;
    observer = Vec3.from(-30,0,50);
  };

  
  Mat4x4.makeIdentity(viewTransform);
  Mat4x4.makeIdentity(modelTransform);
  Mat4x4.makeIdentity(lightTransform);

  //--------------------------------------------------------------------------------------------------------//
  // Set up render loop
  //--------------------------------------------------------------------------------------------------------//

  scene.setViewFrustum(1, 100, 0.5236);

  var animate=function()
  {

    scene.lookAt(observer, [0,0,0], [1,0,0]);
    scene.beginFrame();
    scene.animate();
    scene.draw();
    scene.endFrame();

    window.requestAnimationFrame(animate);
  };

  // Go!
  animate();

};